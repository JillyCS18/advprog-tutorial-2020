package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.HolyGrail;
@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    HolyGrail holyGrail;
    @BeforeEach
    public void setUp() throws Exception {

        holyGrail = new HolyGrail();
        holyGrail.makeAWish("Bismillah");
    }

    @Test
    public void testSetWish() {
        holyGrail.makeAWish("Insya Allah Adpro A");
        assertEquals("Insya Allah Adpro A", holyGrail.getHolyWish().getWish());
    }

    @Test
    public void testGetWish() {
        assertEquals("Bismillah", holyGrail.getHolyWish().getWish());
    }
}
