package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MajesticKnight extends Knight {

    public MajesticKnight(Armory armory) {
        this.armory = armory;
        name = "Majestic Knight";
        switch (armory.getClass().getSimpleName()){
            case "DrangleicArmory":
                name = "Drangleic "+name;
                break;
            case "LordranArmory":
                name = "Lordran "+name;
                break;
        }

    }

    @Override
    public void prepare() {
        // TODO complete me
        armor = armory.craftArmor();
        weapon = armory.craftWeapon();
    }

    public String getDescription() {
        return "Armor and Weapon Specialization";
    }
}
